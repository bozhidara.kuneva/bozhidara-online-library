﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OnlineLibrary.WEB.Startup))]
namespace OnlineLibrary.WEB
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

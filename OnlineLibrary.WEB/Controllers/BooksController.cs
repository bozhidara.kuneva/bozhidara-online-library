﻿using OnlineLibrary.Data.Context;
using OnlineLibrary.Data.Entities;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace OnlineLibrary.WEB.Controllers
{
    public class BooksController : Controller
    {
        private OnlineLibraryDbContext db = new OnlineLibraryDbContext();

        // GET: Books
        public ActionResult Index(int? page, string titleSearch, string sortOrder,string writerSearch,int? GenreId)
        {
            int pageNumber = page ?? 1;
            int pageSize = 5;
            IQueryable<Book> books = db.Books.AsQueryable();

            ViewBag.Genres = new SelectList(db.Genres, "Id", "GenreName",GenreId);
            if ((!String.IsNullOrEmpty(titleSearch) || !String.IsNullOrEmpty(writerSearch) || GenreId.HasValue) && this.User.Identity.IsAuthenticated)
            {
                books = books.Where(x => x.Title.Contains(titleSearch) && x.Writer.UserName.Contains(writerSearch) && x.GenreId == GenreId);
            }
           
            ViewBag.CurrentSortParm = sortOrder;
            ViewBag.TitleSortParm = String.IsNullOrEmpty(sortOrder) ? "title_desc" : "";
            ViewBag.WriterSortParm = sortOrder == "writer_asc" ? "writer_desc" : "writer_asc";
            switch (sortOrder)
            {
                case "title_desc":
                    books = books.OrderByDescending(x => x.Title);
                    break;
                case "writer_asc":
                    books = books.OrderBy(x => x.Writer.UserName);
                    break;
                case "writer_desc":
                    books = books.OrderByDescending(x => x.Writer.UserName);
                    break;
                default:
                    books = books.OrderBy(x => x.Title);
                    break;
            }

            return View(books.ToPagedList(pageNumber, pageSize));
        }

        //GET
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book movie = db.Books.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        //GET
        [Authorize(Roles = "Library Administrator")]
        public ActionResult Create()
        {
            ViewBag.Genres = new SelectList(db.Genres, "Id", "GenreName");
            ViewBag.Writers = new SelectList(db.Writers, "Id", "UserName");

            return View();
        }

        [Authorize(Roles = "Library Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,ReleaseDate,GenreId,WriterId,Description")] Book book)
        {
            if (ModelState.IsValid)
            {
                db.Books.Add(book);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Genres = new SelectList(db.Genres, "Id", "GenreName");
            ViewBag.Writers = new SelectList(db.Writers, "Id", "UserName");

            return View(book);
        }

        //GET
        [Authorize(Roles = "Library Administrator")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book movie = db.Books.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }

            ViewBag.Genres = new SelectList(db.Genres, "Id", "GenreName");
            ViewBag.Writers = new SelectList(db.Writers, "Id", "UserName");

            return View(movie);
        }

        [Authorize(Roles = "Library Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,ReleaseDate,GenreId,WriterId,Description")] Book movie)
        {
            if (ModelState.IsValid)
            {
                db.Entry(movie).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Genres = new SelectList(db.Genres, "Id", "GenreName");
            ViewBag.Writers = new SelectList(db.Writers, "Id", "UserName");

            return View(movie);
        }

        //GET
        [Authorize(Roles = "Library Administrator")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book movie = db.Books.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        [Authorize(Roles = "Library Administrator")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Book book = db.Books.Find(id);
            db.Books.Remove(book);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}